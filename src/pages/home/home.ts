import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage'; 

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
    public noteContent;
    public die1;
    public die2;
    public totalDice;
    public areDiceRolled = false;
    public players:any = [];
    public current_player :number = 0;
    constructor(private storage: Storage, private platform: Platform, public navCtrl: NavController) { 
        this.die1 = 0;
        this.die2 = 0;
        let current_player = +sessionStorage.getItem('current_player'); 
        if(current_player <= 0){
            this.storage.get('current_player').then((val) => {
              this.current_player = val;
              sessionStorage.setItem('current_player', val);
            });
        }
        this.platform.ready().then(() => {
            this.areDiceRolled = false;
            this.loadPlayers();
            this.noteContent = 'Game started.';
            this.current_player = +sessionStorage.getItem('current_player');         
            this.noteContent = this.noteContent + `<br>Player `+this.current_player+`'s turn.`;
        });
          
    }

  rollDice(event) {
      this.die1 = Math.floor(Math.random() * 6) + 1;
      this.die2 = Math.floor(Math.random() * 6) + 1; 
      this.totalDice = this.die1 + this.die2; 
      this.areDiceRolled = true;
      //Dice result
      this.noteContent = this.noteContent + `<br>Player `+this.current_player+` rolled `+this.totalDice+`.`; 
      //
      let nextplayer = this.nextPlayer(this.current_player, this.players); 
      this.noteContent = this.noteContent + `<br>Player `+nextplayer+`'s turn.`;
      this.current_player = nextplayer;
      sessionStorage.setItem('current_player', ""+nextplayer); 
  }

  endTurn(event) {
      this.areDiceRolled = false;
    //console.log(event)
  }

  buy(event) {
      this.areDiceRolled = false;
    //console.log(event)
  }

  loadPlayers() { 
    this.storage.get('players').then((val) => {
         this.players = (val != undefined) ? val : [];
    });  
    /*this.storage.get('current_player').then((val) => { 
          this.current_player = (val != undefined) ? val : 0; 
    }); */ 
  }

  public nextPlayer(current:number, players:any):number{
      let playerNumber = players.length;
      let next = current + 1;
      if(next > playerNumber)
        next = 1;
      return next;
  }

}
